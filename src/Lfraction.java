import java.math.BigDecimal;
import java.util.*;

/** This class represents fractions of form n/d where n and d are long integer
 * numbers. Basic operations and arithmetics for fractions are provided.
 */
public class Lfraction implements Comparable<Lfraction> {

   /** Main method. Different tests. */
   public static void main (String[] param) {
      Lfraction f1 = new Lfraction (-2, 5);
      Lfraction f2 = new Lfraction (4, 7);
      Lfraction f3 = new Lfraction (-4, 75);
      System.out.println(f3);
//      System.out.println(f2.compareTo(f1));
   }

   // TODO!!! instance variables here

   /** Constructor.
    * @param a numerator
    * @param b denominator > 0
    */
   private final long numerator;
   private final long denominator;



   public Lfraction (long a, long b) {
      long denominator1;
      long numerator1;
      if (b < 0){
         numerator1 = a * -1;
         denominator1 = b * -1;
      }
      else if (b == 0){
         throw new RuntimeException("denominator is 0");
      }
      else{
         numerator1 = a;
         denominator1 = b;}


      long great = Math.abs(gcd(numerator1, denominator1));
      this.numerator = numerator1 / great;
      this.denominator = denominator1 / great;
   }

   /** Public method to access the numerator field.
    * @return numerator
    */
   public long getNumerator() {
      return this.numerator;
   }

   /** Public method to access the denominator field.
    * @return denominator
    */
   public long getDenominator() {
      return this.denominator;
   }

   /** Conversion to string.
    * @return string representation of the fraction
    */
   @Override
   public String toString() {
      return this.numerator + "/" + this.denominator;
   }

   /** Equality test.
    * @param m second fraction
    * @return true if fractions this and m are equal
    */
   @Override
   public boolean equals (Object m) {
      if (!(m instanceof Lfraction )){
         return false;
      }
      Lfraction other = new Lfraction(((Lfraction) m).numerator, ((Lfraction) m).denominator);
      return numerator == other.numerator && denominator == other.denominator;
   }


   /** Hashcode has to be equal for equal fractions.
    * @return hashcode
    */
   @Override
   public int hashCode() {
      return Objects.hash(numerator, denominator);
   }

   /** Sum of fractions.
    * @param m second addend
    * @return this+m
    */
   public Lfraction plus (Lfraction m) {
      long big_denominator = this.denominator * m.denominator;
      return new Lfraction(big_denominator / this.denominator * this.numerator +
              big_denominator / m.denominator *  m.numerator, big_denominator);
   }

   /** Multiplication of fractions.
    * @param m second factor
    * @return this*m
    */
   public Lfraction times (Lfraction m) {
      return new Lfraction(numerator * m.numerator, denominator * m.denominator);
   }

   /** Inverse of the fraction. n/d becomes d/n.
    * @return inverse of this fraction: 1/this
    */
   public Lfraction inverse() {
      if (numerator == 0){
         throw new RuntimeException("Cannot divide by 0");
      }
      return new Lfraction(denominator, numerator);
   }

   /** Opposite of the fraction. n/d becomes -n/d.
    * @return opposite of this fraction: -this
    */
   public Lfraction opposite() {
      return new Lfraction(-numerator, denominator);
   }

   /** Difference of fractions.
    * @param m subtrahend
    * @return this-m
    */
   public Lfraction minus (Lfraction m) {
      long big_denominator = this.denominator * m.denominator;
      return new Lfraction(big_denominator / this.denominator * this.numerator -
              big_denominator / m.denominator *  m.numerator, big_denominator);
   }

   /** Quotient of fractions.
    * @param m divisor
    * @return this/m
    */
   public Lfraction divideBy (Lfraction m) {
      if (m.numerator == 0){
         throw new RuntimeException("Cannot divide things with 0");
      }
      return new Lfraction(this.numerator * m.denominator,this.denominator * m.numerator );
   }

   /** Comparision of fractions.
    * @param m second fraction
    * @return -1 if this < m; 0 if this==m; 1 if this > m
    */
   @Override
   public int compareTo (Lfraction m) {
      if (this.equals(m)){return 0;}
      else if ((double) this.numerator / this.denominator < (double) m.numerator / m.denominator){
         return -1;
      }
      return 1;
   }

   /** Clone of the fraction.
    * @return new fraction equal to this
    */
   @Override
   public Object clone() throws CloneNotSupportedException {
      return new Lfraction(this.numerator, this.denominator);
   }

   /** Integer part of the (improper) fraction.
    * @return integer part of this fraction
    */
   public long integerPart() {
      return (long) Math.floor((double) this.numerator / this.denominator);
   }

   /** Extract fraction part of the (improper) fraction
    * (a proper fraction without the integer part).
    * @return fraction part of this fraction
    */
   public Lfraction fractionPart() {
      return new Lfraction(this.numerator % this.denominator,this.denominator);
   }

   /** Approximate value of the fraction.
    * @return numeric value of this fraction
    */
   public double toDouble() {
      return this.numerator * 1.0 / this.denominator;
   }

   /** Double value f presented as a fraction with denominator d > 0.
    * @param f real number
    * @param d positive denominator for the result
    * @return f as an approximate fraction of form n/d
    */
   public static Lfraction toLfraction (double f, long d) {
      return new Lfraction((long) Math.ceil( f * d), d);
   }

   /**
    * Conversion from string to the fraction. Accepts strings of form
    * that is defined by the toString method.
    *
    * @param s string form (as produced by toString) of the fraction
    * @return fraction represented by s
    */
   public static Lfraction valueOf(String s) {


      String[] strings = s.split("/");
      if (strings.length != 2){
         throw new RuntimeException("String lenght must be 2");
      }
      return new Lfraction(Long.parseLong(strings[0]), Long.parseLong(strings[1]));
   }
   private static long gcd(long a, long b) { //https://stackoverflow.com/questions/6618994/simplifying-fractions-in-java
      return b == 0 ? a : gcd(b, a % b);
   }

}